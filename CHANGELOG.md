## [1.5.9](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.8...1.5.9) (2025-01-24)


### Bug Fixes

* **deps:** update poetry dependencies ([75e502e](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/75e502e16c0445f7ffa69ccf96250b578ebf037d))

## [1.5.8](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.7...1.5.8) (2025-01-10)


### Bug Fixes

* **deps:** update poetry dependencies ([f21b66b](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f21b66b291ba6972c053becb305b906c3c9d6a8e))

## [1.5.7](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.6...1.5.7) (2024-12-20)


### Bug Fixes

* **deps:** update poetry dependencies ([789c462](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/789c46223db4d8043d178b92a0141e95ccafd0b5))

## [1.5.6](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.5...1.5.6) (2024-12-06)


### Bug Fixes

* **deps:** update poetry dependencies ([4706718](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/47067185a25416c3901f8dc82e326abf7f114892))

## [1.5.5](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.4...1.5.5) (2024-11-29)


### Bug Fixes

* **deps:** update poetry dependencies ([6f03953](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/6f0395385499e678294269553e2bec5418f82896))

## [1.5.4](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.3...1.5.4) (2024-11-22)


### Bug Fixes

* **deps:** update poetry dependencies ([70bf494](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/70bf494b17e1cd7a1df7fa16a120112e48105c08))

## [1.5.3](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.2...1.5.3) (2024-09-20)


### Bug Fixes

* **deps:** update poetry dependencies ([1eb1daa](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/1eb1daaf452e7cd11c6ebc7d1d0ab7f4076e4f6c))

## [1.5.2](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.1...1.5.2) (2024-09-13)


### Bug Fixes

* **deps:** update poetry dependencies ([ed1732e](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/ed1732ea523561c3c9625ad0a8124fc30e578b12))

## [1.5.1](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.5.0...1.5.1) (2024-09-06)


### Bug Fixes

* **deps:** update poetry dependencies ([04ee4b5](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/04ee4b5da04159ac5dbe60ccb7f59dad21233df9))

# [1.5.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.9...1.5.0) (2024-08-25)


### Features

* add jacoco coverage report format support ([e4c4436](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/e4c4436d23980a94185a41ae3ca69895364aeaad))

## [1.4.9](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.8...1.4.9) (2024-08-19)


### Bug Fixes

* tbc-check fails when the reports block is empty (is it legit?) ([19cfe64](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/19cfe64193fd9ad6bb6a8c94b426053b87819f97)), closes [#8](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/8)

## [1.4.8](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.7...1.4.8) (2024-08-09)


### Bug Fixes

* **deps:** update poetry dependencies ([ddb7765](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/ddb776503769761122781911ae30c27b43ab0eb8))

## [1.4.7](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.6...1.4.7) (2024-08-05)


### Bug Fixes

* a variable/input override (in a variant) is always ignored ([9208081](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/9208081f452e8ef398256c1f15b12a1a964f67c4))

## [1.4.6](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.5...1.4.6) (2024-07-05)


### Bug Fixes

* **deps:** update dependency pydantic to v2.8.2 ([77a7f17](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/77a7f1720cb3284ebdbfdeed4b483792cb2bc033))

## [1.4.5](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.4...1.4.5) (2024-06-14)


### Bug Fixes

* **deps:** update dependency pydantic to v2.7.4 ([f4a01bc](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f4a01bcf69fa704e8792dea10598bf55f3e6e856))

## [1.4.4](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.3...1.4.4) (2024-06-07)


### Bug Fixes

* **deps:** update poetry dependencies ([16146de](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/16146de615961f4b83e70b5dc7c5e2c6f42944e4))

## [1.4.3](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.2...1.4.3) (2024-06-02)


### Bug Fixes

* skip checking YAML alias as job ([3922d9d](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/3922d9d13231980c2dc53eea50114245ec072c55))

## [1.4.2](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.1...1.4.2) (2024-05-31)


### Bug Fixes

* **deps:** update poetry dependencies ([d89dd33](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/d89dd3324dc51dfc5ca1d794fef22d0e7d423353))

## [1.4.1](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.4.0...1.4.1) (2024-05-18)


### Bug Fixes

* use generic TBC renovate config ([46a3673](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/46a36731cf0982535921ea5005ee53071c48b1d8))

# [1.4.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.3.1...1.4.0) (2024-05-05)


### Bug Fixes

* turn single job template inheritance rule into warning (for make) ([3352dea](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/3352dea91f51c62faf4370d39d37efff8785d957))


### Features

* skip OK message if at least one warn ([741cbae](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/741cbaec7be607f74c8b4fed39f9397d86a3f8d0))

## [1.3.1](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.3.0...1.3.1) (2024-05-05)


### Bug Fixes

* don't test job prefix rule on "pages" job (special) ([0ea7702](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/0ea770281a761b2137cca3129743f060f5d69a5c))

# [1.3.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.2.0...1.3.0) (2024-05-05)


### Features

* skip partial job definition and add explicit output messages for each job ([f95a821](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f95a8219951707122ea30f11ff03ed0fa33d91e7))

# [1.2.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.1.0...1.2.0) (2024-05-05)


### Features

* implement job-prototype and inheritance check ([1effdd1](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/1effdd1e8cfeabbbccdeb7e55a76c26db680ae36)), closes [#7](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/7)

# [1.1.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.0.0...1.1.0) (2024-04-27)


### Bug Fixes

* pydantic parse_obj deprecation warning ([ea50ac4](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/ea50ac42e094e73700cba3e9ec497126ab9a723d))


### Features

* check container image rules ([f7f54bb](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f7f54bba300ed786d0eb92dda70d4bfa206d5365)), closes [#5](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/5) [#6](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/6)
* check emtpy default can be omitted in Kicker ([d19c780](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/d19c780e5a4648b6ee0199eee19636158ceec69b)), closes [#2](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/2)
* check jobs prefix rule ([06fa347](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/06fa3473493d0e2a15900bcb44344217819c6f50)), closes [#4](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/4)
* check report naming conventions ([f258fa8](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f258fa877d1683382b49bb823711f71d02513c15)), closes [#3](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/3)
