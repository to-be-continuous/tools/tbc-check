FROM registry.hub.docker.com/library/python:3.13-alpine

ENV PORT=80
WORKDIR /code

COPY ./dist/*.whl /code/

RUN apk upgrade --no-cache \
    && pip install --no-cache-dir /code/*.whl

ENTRYPOINT [ "tbc-check" ]
CMD [ "." ]