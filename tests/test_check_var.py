import helper
import pytest

from tbc_check import checker

KICKER_VAR_STRING = helper.make_var_fixture(
    name="A_STRING",
    description="string var",
    type=checker.TbcVarType.text,
    default="some_value",
)

KICKER_VAR_STRING_W_EMPTY_DEFAULT = helper.make_var_fixture(
    name="A_STRING", description="string var", type=checker.TbcVarType.text, default=""
)

KICKER_VAR_IMAGE = helper.make_var_fixture(
    name="TEST_IMAGE",
    description="test image",
    type=checker.TbcVarType.text,
    default="registry.hub.docker.com/library/test:latest",
)

KICKER_VAR_IMAGE_W_NO_REGISTRY = helper.make_var_fixture(
    name="TEST_IMAGE",
    description="test image",
    type=checker.TbcVarType.text,
    default="test:latest",
)

KICKER_VAR_IMAGE_W_UNCOMMON_HOST = helper.make_var_fixture(
    name="TEST_IMAGE",
    description="test image",
    type=checker.TbcVarType.text,
    default="registry.acme.tbc/test:latest",
)


def test_check_var_a_string(capfd: pytest.CaptureFixture[str]):
    res = checker._check_var(**KICKER_VAR_STRING)
    out, err = capfd.readouterr()
    assert out == ("  \x1b[0;32m✓\x1b[0m <A_STRING/a-string>: OK\n")


def test_check_var_a_string_w_empty_default(capfd: pytest.CaptureFixture[str]):
    res = checker._check_var(**KICKER_VAR_STRING_W_EMPTY_DEFAULT)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;31m✕ <A_STRING>: empty default shall be omitted in Kicker\x1b[0m\n"
    )


def test_check_var_image_registry_good(capfd: pytest.CaptureFixture[str]):
    res = checker._check_var(**KICKER_VAR_IMAGE)
    out, err = capfd.readouterr()
    assert out == ("  \x1b[0;32m✓\x1b[0m <TEST_IMAGE/test-image>: OK\n")


def test_check_var_image_registry_bad(capfd: pytest.CaptureFixture[str]):
    res = checker._check_var(**KICKER_VAR_IMAGE_W_NO_REGISTRY)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;31m✕ <TEST_IMAGE>: container images must explicitly specify the registry\x1b[0m\n"
    )


# def test_check_var_image_registry_uncommon(capfd: pytest.CaptureFixture[str]):
#     res = checker._check_var(**KICKER_VAR_IMAGE_W_UNCOMMON_HOST)
#     out, err = capfd.readouterr()
#     assert out == (
#         "  \x1b[0;33m⚠ <TEST_IMAGE/test-image> uses a less common registry host 'registry.acme.tbc'\x1b[0m\n"
#         "  \x1b[0;32m✓\x1b[0m <TEST_IMAGE/test-image>: OK\n"
#     )
