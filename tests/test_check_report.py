import pytest

from tbc_check import checker


def test_check_report_junit_ok1(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "reports/test.xunit.xml")
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_junit_ok2(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "$TEST_DIR/reports/test.xunit.xml")
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_coverage_report_ok(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "coverage_report", "reports/test.cobertura.xml")
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_sbom_ok(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("sbom", "cyclonedx", "reports/sbom.cyclonedx.json")
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_codequality_ok(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report(
        "linter", "codequality", "reports/linter.codeclimate.json"
    )
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_scan_ok(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report(
        "trivy", "container_scanning", "reports/trivy.gitlab.json"
    )
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_k6_ok(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("k6", "load_performance", "reports/k6.summary.json")
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_report_unsupported_type(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "unsupported", "reports/test.unsupported.xml")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[90m? report 'reports/test.unsupported.xml' (unsupported): unsupported report type\x1b[0m\n"
    )


def test_check_report_wrong_filename(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "$TEST_DIR/reports/mytest.xunit.xml")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;33m⚠ report '$TEST_DIR/reports/mytest.xunit.xml' (junit): filename (no extension) should preferably start with 'test'\x1b[0m\n"
    )


def test_check_report_2_parts(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "reports/test.xml")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;33m⚠ report 'reports/test.xml' (junit): filename should preferably be in 3 parts '<job-name>.<format>.<extension>'\x1b[0m\n"
    )


def test_check_report_wrong_pre_ext(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "reports/test.aunit.xml")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;31m✕ report 'reports/test.aunit.xml' (junit): filename middle part must match 'junit|xunit'\x1b[0m\n"
    )


def test_check_report_wrong_ext(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "reports/test.xunit.json")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;31m✕ report 'reports/test.xunit.json' (junit): file extension must match 'xml'\x1b[0m\n"
    )


def test_check_report_wrong_folder(capfd: pytest.CaptureFixture[str]):
    res = checker._check_report("test", "junit", "output/test.xunit.xml")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;33m⚠ report 'output/test.xunit.xml' (junit): should preferably be generated in a 'reports/' folder\x1b[0m\n"
    )
